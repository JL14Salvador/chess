#include <iostream>
#include "chess.h"

using namespace std; 

int main() {

///Welcome Screen
cout << "This is Chess!" << endl; 

///Construct a chess object that implements game
Chess chess(6,6); 

///Start the game and run loop 
chess.play(); 

return 0; 

}