#ifndef __GAME_H_INCLUDED__
#define __GAME_H_INCLUDED__

#include <iostream>
#include "board.h"
#include "square.h" 

using namespace std; 

class Game {
 public:  	
  	///Constructors & Destructors
  	Game(){}
  	virtual ~Game(){} 
  	Board* b; 

  	///Functions
  	void play(); 
  	virtual void setup() = 0; 
  	virtual bool isOver() = 0; 
  	virtual Square* getSquare(istream &is) = 0; 
  	bool movePiece(Square* s, Square* d); 

 private: 

};

#endif

