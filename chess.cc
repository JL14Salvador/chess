#include "chess.h" 
#include <assert.h>

////Will create a 6x6 board and place the pieces on the board as well 
Chess::Chess(int x, int y) {
	b = new Board(x,y); 

	///Assign rooks for white
	wRook1 = new Piece(1, 'r');
	wRook2 = new Piece(1, 'r');

	///Assign bishops for white 
	wBish1 = new Piece(1, 'b');
	wBish2 = new Piece(1, 'b');

	///Assign pawns for white 
	wPawn1 = new Piece(1, 'p');
	wPawn2 = new Piece(1, 'p'); 
	wPawn3 = new Piece(1, 'p'); 
	wPawn4 = new Piece(1, 'p'); 
	wPawn5 = new Piece(1, 'p'); 
	wPawn6 = new Piece(1, 'p'); 

	///Assign queen for white
	wQueen = new Piece(1, 'q');

	///Assign King for white
	wKing = new Piece(1, 'k');

	///Assign rooks for black
	bRook1 = new Piece(2, 'R');
	bRook2 = new Piece(2, 'R');

	///Assign bishops for black 
	bBish1 = new Piece(2, 'P');
	bBish2 = new Piece(2, 'P');

	///Assign pawns for black 
	bPawn1 = new Piece(2, 'P');
	bPawn2 = new Piece(2, 'P'); 
	bPawn3 = new Piece(2, 'P'); 
	bPawn4 = new Piece(2, 'P'); 
	bPawn5 = new Piece(2, 'P'); 
	bPawn6 = new Piece(2, 'P'); 

	///Assign queen for black 
	bQueen = new Piece(2,'Q'); 
	
	///Assign king for black 
	bKing = new Piece(2, 'K'); 


}

Chess::~Chess(){

 	delete wRook1;
 	delete wRook2;
 	delete wBish1;
 	delete wBish2;
 	delete wPawn1;
 	delete wPawn2; 
 	delete wPawn3; 
 	delete wPawn4; 
 	delete wPawn5; 
 	delete wPawn6; 
 	delete wQueen;
 	delete wKing;
 	delete bRook1;
 	delete bRook2;
 	delete bBish1;
 	delete bBish2;
 	delete bPawn1;
 	delete bPawn2; 
 	delete bPawn3; 
 	delete bPawn4; 
 	delete bPawn5; 
 	delete bPawn6; 
 	delete bQueen; 
 	delete bKing;

 	delete b;
}

///This function populates the board with the pieces 
void Chess::setup() {

	///Place pieces white pieces on the board 
	b->placePiece(wRook1, b->getSquare(0,0) ); 
	b->placePiece(wBish1, b->getSquare(0,1) );
	b->placePiece(wKing, b->getSquare(0,2) );
	b->placePiece(wQueen, b->getSquare(0,3) );
	b->placePiece(wBish2, b->getSquare(0,4) );
	b->placePiece(wRook2, b->getSquare(0,5) );
	b->placePiece(wPawn1, b->getSquare(1,0) ); 
	b->placePiece(wPawn2, b->getSquare(1,1) ); 
	b->placePiece(wPawn3, b->getSquare(1,2) ); 
	b->placePiece(wPawn4, b->getSquare(1,3) ); 
	b->placePiece(wPawn5, b->getSquare(1,4) ); 
	b->placePiece(wPawn6, b->getSquare(1,5) ); 

	///Place black pieces on the board 
	b->placePiece(bRook1, b->getSquare(5,0) ); 
	b->placePiece(bBish1, b->getSquare(5,1) );
	b->placePiece(bKing, b->getSquare(5,2) );
	b->placePiece(bQueen, b->getSquare(5,3) );
	b->placePiece(bBish2, b->getSquare(5,4) );
	b->placePiece(bRook2, b->getSquare(5,5) );
	b->placePiece(bPawn1, b->getSquare(4,0) ); 
	b->placePiece(bPawn2, b->getSquare(4,1) ); 
	b->placePiece(bPawn3, b->getSquare(4,2) ); 
	b->placePiece(bPawn4, b->getSquare(4,3) ); 
	b->placePiece(bPawn5, b->getSquare(4,4) ); 
	b->placePiece(bPawn6, b->getSquare(4,5) ); 
}

///Signifies if the game is over
bool Chess::isOver() {

	if(bKing->isAlive() && wKing->isAlive()){
		return false; 
	}
	else 
		cout << "Game is over" << endl; 
		return true; 	
}

///Receieves input from a user to define the two variables to pass to the square object and then returns that object
Square* Chess::getSquare(istream &is){
	int x, y; 
	is >> x >> y; 
	assert(x >= 0);
	assert(y >= 0);
	assert(x < 6);
	assert(x < 6);

		return b->getSquare(x,y); 
}