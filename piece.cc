#include "piece.h" 
#include <iostream>

///Constructs a piece object with a color, symbol and a status if it is alive or not
Piece::Piece(int co, char sy){
	color = co; 
	symbol = sy;
	active = true;  
}

///Checks to see if a piece is alive, returns its status
bool Piece::isAlive() {
	return active; 
}

///Sets the piece's status to not active
void Piece::kill() {
	active = false; 
}

///Return the symbol of the piece 
char Piece::getSymbol() { 
	return symbol; 
}