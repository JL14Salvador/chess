#ifndef __SQUARE_H_INCLUDED__
#define __SQUARE_H_INCLUDED__

#include "piece.h"

class Square {
 public:
  ///Constructors & Destructors
  Square(int r, int c);   

 ///Functions 
  char symbol() ; 
  void setPiece(Piece* p); 
  Piece* getPiece(); 
  Piece* removePiece();

 private:
  int row; 
  int col; 
  Piece* piece;
};

#endif
