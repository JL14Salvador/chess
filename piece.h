#ifndef __PIECE_H_INCLUDED__
#define __PIECE_H_INCLUDED__

class Piece {
 public:
 	///Constructors & Destructors
 	Piece(int co, char sy);

  	///Functions
  	bool isAlive();
 	void kill(); 
 	char getSymbol();

 private:
 	int color; 
 	char symbol; 
 	bool active; 
};

#endif


