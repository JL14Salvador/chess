#ifndef __CHESS_H_INCLUDED
#define __CHESS_H_INCLUDED

#include "game.h"

using namespace std;

class Chess : public Game {
 public: 

 ///Constructors & Destructors
  Chess(int x, int y); 
  ~Chess();

///Functions
  void setup(); 
  bool isOver();
  Square* getSquare(istream &is);  

 private: 
 	///Create rooks for white
 	Piece* wRook1;
 	Piece* wRook2;

 	///Create bishops for white 
 	Piece* wBish1;
 	Piece* wBish2;

 	///Create pawns for white 
 	Piece* wPawn1;
 	Piece* wPawn2; 
 	Piece* wPawn3; 
 	Piece* wPawn4; 
 	Piece* wPawn5; 
 	Piece* wPawn6; 

 	///Create queen for white
 	Piece* wQueen;

 	///Create King for white
 	Piece* wKing;

 	///Create rooks for black
 	Piece* bRook1;
 	Piece* bRook2;

 	///Create bishops for black 
 	Piece* bBish1;
 	Piece* bBish2;

 	///Create pawns for black 
 	Piece* bPawn1;
 	Piece* bPawn2; 
 	Piece* bPawn3; 
 	Piece* bPawn4; 
 	Piece* bPawn5; 
 	Piece* bPawn6; 

 	///Create queen for black 
 	Piece* bQueen; 
 	
 	///Create king for black 
 	Piece* bKing; 
};

#endif


