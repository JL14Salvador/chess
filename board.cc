#include "board.h"
#include <iostream>

///This will draw the board to the ostream
void Board::draw(ostream *o) {
 	for(int i=0;i<6;i++) {
 		*o << "-" << endl; 
 		for(int j=0; j<6; j++) {
 			*o << "|" << panel[i][j]->symbol(); 
 			if(j==5)
 				*o << "|"; 
 		}
 		if(i==5)
 			*o << endl; 
 	}
}

///take what piece is pointing to and point it to square s
void Board::placePiece(Piece* p, Square* s) {
	s->setPiece(p);
}

///This will move a piece from square s to square d and kill what is on d if it is alive
void Board::movePiece(Square* s, Square* d) {
	if(d->getPiece() != NULL)
		d->removePiece()->kill(); 		
	d->setPiece(s->removePiece() );  
}

///returns Square at the location on the board 
Square* Board::getSquare(int r, int c) {
   return panel[r][c];
}



