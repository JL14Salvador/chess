#include "square.h"
#include <iostream>

///Constructs a square with the parameters row and column and sets piece to null
Square::Square(int r, int c){
   row = r; 
   col = c;  
   piece = NULL;
}

///return the symbol in square, will return a '.' if piece is null
char Square::symbol() {
   if(piece == NULL)
   		return '.';
   else
   		return piece->getSymbol();  
}

///sets piece to the private variable piece in the class
void Square::setPiece(Piece* p){
  piece = p; 
}

///returns a piece
Piece* Square::getPiece(){
	if(piece == NULL)
		return NULL;
	else
	  return piece; 
}

///This will create a new temp piece, make the old piece point to nothing and return temp
Piece* Square::removePiece(){

	Piece* temp = piece;
	piece = NULL;
	return temp;  
}