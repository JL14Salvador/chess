#include "game.h"
#include <assert.h>

///This function calls setup() and will continue to ask for input until one of the kings die
void Game::play() {

	setup(); 
	while(!isOver() ) {
		b->draw(&cout); 
		Square* start;
		Square* end; 	
		cout <<"Please select a piece: "; 
		start = getSquare(cin); 
		assert(start->symbol() != '.');

		cout << "\nPlease select where you would like to move it: ";
		end = getSquare(cin);
		assert(start != end);

		b->movePiece(start, end); 
	}
}

///a function that removes a piece from the square s and place the square at d
bool Game::movePiece(Square* s, Square* d){
	b->movePiece(s, d);
  return true;
}





