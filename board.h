#ifndef __BOARD_H_INCLUDED__
#define __BOARD_H_INCLUDED__

#include <iostream>
#include <vector> 
#include "piece.h" 
#include "square.h"

using namespace std; 

class Board {
 public: 
  ///Constructors & Destructors
  Board(int w, int h) 
    : width(w), height(h), panel(height, vector<Square*>(width, NULL))
  {
     for(int r=0; r<height; r++) {
      for(int c=0; c<width;c++)
	     panel[r][c] = new Square(r,c);
    }
  }  
  ~Board(){
    for(int r=0; r<height; r++) {
      for(int c=0; c<width; c++)
        delete panel[r][c];
    }
  }

  ///Functions
  void draw(ostream *o); 
  void placePiece(Piece* p, Square* s);
  void movePiece(Square* s, Square* d);
  Square* getSquare(int r, int c); 

  private:
  int width;
  int height; 
  vector< vector<Square*> > panel;  
};

#endif
